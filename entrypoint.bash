#!/bin/bash

if [[ -n ${SSH_PRIVATE_KEY} ]]; then
  echo -e "\e[1;32mAdding private ssh key from SSH_PRIVATE_KEY variable\e[m"
  eval "$(ssh-agent -s)"
  echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add -
  unset SSH_PRIVATE_KEY
fi

/bin/bash "$@"
