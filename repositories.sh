#!/bin/sh
set -eu

BRANCH=$1

if [ "$BRANCH" = 'edge' ]; then
	cat repositories.edge
else  # >= v3.16
	esh repositories.stable.esh BRANCH=$BRANCH
fi
